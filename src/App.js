import React, { Component } from 'react';
import './App.css';
import Panel from 'react-bootstrap/lib/Panel';
import {PanelGroup} from 'react-bootstrap';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Modal from 'react-bootstrap/lib/Modal';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormControl from 'react-bootstrap/lib/FormControl';

class App extends Component {

  state = {
    recipes: [
      // {
      //   recipeName: 'Shepard\'s Pie',
      //   ingredients: ['peas', 'corn', 'mushrooms', 'mashed potatoes'] 
      // },
      // {
      //   recipeName: 'Smoothie Bowl',
      //   ingredients: ['berries', 'strawberries', 'banana', 'nuts', 'coconut pieces'] 
      // },
      // {
      //   recipeName: 'Cocktail Drink',
      //   ingredients: ['lime', 'ice', 'strawberry'] 
      // }
    ],
    showAdd: false,
    showEdit: false,
    currentIndex: 0,
    newestRecipe: {
      recipeName:'',
      ingredients: []
    }
  }

  // Deletes a recipe
  deleteRecipe(index) {
    let recipes = this.state.recipes.slice();
    recipes.splice(index, 1);

    localStorage.setItem('recipes', JSON.stringify(recipes));
    this.setState({recipes});
  }

  // Update newRecipe.recipeName
  updateNewRecipe(recipeName, ingredients) { 

    this.setState({newestRecipe: {recipeName: recipeName, ingredients: ingredients}});
  }

  saveNewRecipe(newestRecipe) {
    let recipes = this.state.recipes.slice();
    recipes.push(newestRecipe);

    localStorage.setItem('recipes', JSON.stringify(recipes));
    this.setState({recipes});
    this.setState(
      {
        newestRecipe: {
          recipeName:'',
          ingredients: []
        }
      }
    )
    this.close();
  }

  // Opens modal
  open = (state, currentIndex) => {
    this.setState({[state]: true});
    this.setState({currentIndex});
  }

  // Closes modal
  close = () => {
    if(this.state.showAdd) {
      this.setState({showAdd:false})
    }
    if(this.state.showEdit) {
      this.setState({showEdit:false})
    }
  }

  // Updates recipe name
  updateRecipeName(recipeName, currentIndex) {
    let recipes = this.state.recipes.slice();

    recipes[currentIndex] = {
      recipeName: recipeName, 
      ingredients: recipes[currentIndex].ingredients
    };

    localStorage.setItem('recipes', JSON.stringify(recipes));
    this.setState({recipes});
  }

  // Updates ingredients
  updateIngredients(ingredients, currentIndex) {
    let recipes = this.state.recipes.slice();

    recipes[currentIndex] = {
      recipeName: recipes[currentIndex].recipeName, 
      ingredients: ingredients
    };

    localStorage.setItem('recipes', JSON.stringify(recipes));
    this.setState({recipes});
  }
  
  saveEditedRecipe() {
    this.close();
  }

  componentDidMount() {
    let recipes = JSON.parse(localStorage.getItem('recipes')) || [];
    this.setState({recipes});
  }

  render() {
    const {recipes, newestRecipe, currentIndex} = this.state;
    console.log(newestRecipe);

    return (
      <div className="App container">
        <h1>Recipe Box</h1>
      
        {recipes.length > 0 && (
          <div>
            <PanelGroup accordion id="recipe-box">
              {recipes.map((recipe, index) => {
                return(
                  <Panel eventKey={index} key={index}>
                    <Panel.Heading>
                      <Panel.Title toggle>{recipe.recipeName}</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body collapsible>
                      <ul>
                        {recipe.ingredients.map((ingredient, index) => {
                          return (
                            <li key={index}>{ingredient}</li>
                          )
                        })}
                      </ul>
                      <ButtonToolbar>
                        <Button 
                          bsStyle='default'
                          onClick={(event) => this.open("showEdit", index)}
                        >Edit</Button>
                        <Button 
                          bsStyle='danger' 
                          onClick={(event) => this.deleteRecipe(index)}
                        >Delete</Button>
                      </ButtonToolbar>
                    </Panel.Body>
                  </Panel>             
                )             
              })}   
            </PanelGroup>
         

          <Modal show={this.state.showEdit} onHide={this.close}>
            <Modal.Header closeButton>
              <Modal.Title>Edit Recipe</Modal.Title>
            </Modal.Header>  

            <Modal.Body>
              <FormGroup controlId='formBasicText'>
                <ControlLabel>Recipe Name:</ControlLabel>
                <FormControl
                  type='text'
                  value={recipes[currentIndex].recipeName}
                  placeholder="Enter Recipe Name"
                  onChange={(event) => this.updateRecipeName(event.target.value, currentIndex)}
                >
                </FormControl>
              </FormGroup>
              <FormGroup controlId='formControlsTextArea'>
                <ControlLabel>Ingredients:</ControlLabel>
                <FormControl
                  type='textarea'
                  value={recipes[currentIndex].ingredients}
                  placeholder="Enter Ingredients (Seperate By Commas)"
                  onChange={(event) => this.updateIngredients(event.target.value.split(","), currentIndex)}
                >
                </FormControl>
              </FormGroup>   
            </Modal.Body>     

            <Modal.Footer>
              <Button
                onClick={(event) => this.saveEditedRecipe()}
              >Save</Button>
            </Modal.Footer>   
          </Modal>
          </div> 
        )}
         
        

        <Modal show={this.state.showAdd} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Add Recipe</Modal.Title>
          </Modal.Header>  
            <Modal.Body>
              <FormGroup controlId='formBasicText'>
                <ControlLabel>Recipe Name:</ControlLabel>
                <FormControl
                  type='text'
                  value={this.state.newestRecipe.recipeName}
                  placeholder="Enter Recipe Name"
                  onChange={(event) => this.updateNewRecipe(event.target.value, newestRecipe.ingredients)}
                >
                </FormControl>
              </FormGroup>
              <FormGroup controlId='formControlsTextArea'>
                <ControlLabel>Ingredients:</ControlLabel>
                <FormControl
                  type='textarea'
                  value={newestRecipe.ingredients}
                  placeholder="Enter Ingredients (Seperate By Commas)"
                  onChange={(event) => this.updateNewRecipe(newestRecipe.recipeName, event.target.value.split(","))}
                >
                </FormControl>
              </FormGroup>   
            </Modal.Body>     

            <Modal.Footer>
              <Button
                onClick={(event) => this.saveNewRecipe(newestRecipe)}
              >Save</Button>
            </Modal.Footer>   
        </Modal>

        <Button 
          bsStyle='primary'
          onClick={(event) => this.open("showAdd", currentIndex)}
        >Add Recipe</Button>

        <footer>
          <p>Jennifer Currie &copy;2018 <a href="http://renestl.github.io/" target='_blank' rel='noopener noreferrer'>Portfolio</a></p>
        </footer>
      </div>
    );
  }
}

export default App;
